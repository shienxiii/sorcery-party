// Author: Thau Shien Hsu

#include "PickUpChip.h"


// Sets default values
APickUpChip::APickUpChip()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APickUpChip::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickUpChip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

