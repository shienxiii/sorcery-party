// Author: Thau Shien Hsu

#include "SorcererCharacterBase.h"


// Sets default values
ASorcererCharacterBase::ASorcererCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_playerAura = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MovementParticles"));
	_playerAura->SetupAttachment(this->RootComponent);

	_locomotionSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Locomotion Sound"));
	_locomotionSound->SetupAttachment(this->RootComponent);

	_castingSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Casting sound"));
	_castingSound->SetupAttachment(this->RootComponent);
}

// Called when the game starts or when spawned
void ASorcererCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	_downTimerCurrent = _downTimerBase;
	_movementSpeed = GetCharacterMovement()->MaxWalkSpeed;
}


// Called every frame
void ASorcererCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CastingUpdate(DeltaTime);
}

///<summary>
/// Takes care of all magic casting related calculations, shoule be called every frame. Have same parameter as Tick()
///</summary>
void ASorcererCharacterBase::CastingUpdate(float DeltaTime)
{
	if (_castCurrentCooldown > 0.0f)
	{
		_castCurrentCooldown -= DeltaTime;
	}
	else if (_castCurrentCooldown <= 0.0f && _cast)
	{
		Cast();
		_castCurrentCooldown = _castCooldown;
	}
}

// Called to bind functionality to input
void ASorcererCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void ASorcererCharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	_controllingPlayer = (APlayerController*) NewController; // DIRTY FIX
}



///<summary>
/// Called when first spawned to initialize pointers necessary for the game and player character communication
///</summary>
///<param name="inGameCamera">The game camera that all players will view this the game from</param>
///<param name="inSpawnPoint">This character's initial spawn point</param>
void ASorcererCharacterBase::SpawnInitialization(AGameCamera* inGameCamera, APlayerStart* inSpawnPoint, UMaterialInstance* inThumbnail, float inMana)
{
	_gameCamera = inGameCamera;
	_spawnPoint = inSpawnPoint;
	_thumbnail = inThumbnail;
	_mana = inMana;
}


///<summary>
/// Called by crystal when the crystal is tagged to put player into Enhanced Mode
///</summary>
void ASorcererCharacterBase::CrystalTagEvent()
{
	GetCharacterMovement()->MaxWalkSpeed = _movementSpeed * _movementSpeedMod;
	//_playerAura->Activate();
}

///<summary>
/// Called by crystal when it burst or tagged by another player to deactivated Enhanced Mode
///</summary>
void ASorcererCharacterBase::CrystalUntagEvent()
{
	GetCharacterMovement()->MaxWalkSpeed = _movementSpeed;
	//_playerAura->Deactivate();
}


///<summary>
/// Add input to player mana/score
///</summary>
///<param name="inMana">Mana to add to the player</param>
float ASorcererCharacterBase::AddMana(float inMana)
{
	_mana = FMath::Clamp((_mana += inMana), 0.0f, 100.0f);
	return _mana;
}


UMaterialInstance * ASorcererCharacterBase::GetCrystalTagMat()
{
	return _crystalTagMat;
}


void ASorcererCharacterBase::SetCrystalTagMat(UMaterialInstance * inCrystalTagMat)
{
	_crystalTagMat = inCrystalTagMat;
}


///<summary>
/// Cast the assigned Projectile object
///</summary>
void ASorcererCharacterBase::Cast()
{
	FTransform castPoint = FTransform(_magicCircle->GetComponentLocation());
	castPoint.SetRotation(GetActorRotation().Quaternion());

	AProjectiles* casted = GetWorld()->SpawnActorDeferred<AProjectiles>(_projectile, castPoint);

	if (casted != NULL)
	{
		casted->Initialize(this);
	}

	UGameplayStatics::FinishSpawningActor(casted, castPoint);
}


///<summary>
/// Called by a Magic Chip when a player overlaps with it(pick it up)
///</summary>
///<param name="inMagicCircleMat">Magic Circle material to represent the new magic</param>
///<param name="inMagicProjectile">The magic ability that the player will use</param>
void ASorcererCharacterBase::ChangeMagic(UMaterialInterface* inMagicCircleMat, TSubclassOf<AProjectiles> inMagicProjectile)
{
	_magicCircle->SetMaterial(0, inMagicCircleMat);
	_projectile = inMagicProjectile;
}


///<summary>
/// Registers the damage players will receive
///</summary>
///<param name="inDamage">Damage value the player receive</param>
void ASorcererCharacterBase::ReceiveDamage(float inDamage)
{
	_tookDamage = true;

	_hp -= inDamage;

	if (_hp <= 0.0f)
	{
		Incapacitated();
	}

	
}


//<summary>
/// Called when HP reach 0
///</summary>
void ASorcererCharacterBase::DamageAnimComplete()
{
	_tookDamage = false;
}


///<summary>
/// Called when HP reach 0
///</summary>
void ASorcererCharacterBase::Incapacitated()
{
	DisableInput(_controllingPlayer);

	_gameCamera->RemoveActor(this);

	_cast = false;

	//SetActorEnableCollision(false);
}

///<summary>
/// Called when player falls off the map
///</summary>
void ASorcererCharacterBase::DisableCharacter()
{
	_hp = 0.0f;
	_cast = false;

	DisableInput(_controllingPlayer);

	_gameCamera->RemoveActor(this);

	isHidden = true;
	
	SetActorHiddenInGame(true);
	//SetActorEnableCollision(false);
}


///<summary>
/// Called when revives
///</summary>
void ASorcererCharacterBase::ResetStats()
{
	EnableInput(_controllingPlayer);

	_gameCamera->RegisterActor(this);

	_hp = 100.0f;
	_downTimerCurrent = _downTimerBase;

	if (isHidden)
	{
		SetActorTransform(_spawnPoint->GetActorTransform());
		SetActorHiddenInGame(false);
		isHidden = false;
	}

	
	//SetActorEnableCollision(true);
}

 float ASorcererCharacterBase::GetHPRatio()
{
	 return _hp / _maxHP;
}

 float ASorcererCharacterBase::GetManaRatio()
 {
	 return _mana / _maxMana;
 }

 float ASorcererCharacterBase::GetMana()
 {
	 return _mana;
 }

AProjectiles* ASorcererCharacterBase::GetProjectileClassReference()
 {
	return _projectile.GetDefaultObject();
 }
