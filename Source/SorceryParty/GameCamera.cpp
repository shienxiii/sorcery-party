// Author: Thau Shien Hsu

#include "GameCamera.h"



AGameCamera::AGameCamera()
{
	PrimaryActorTick.bCanEverTick = true;
	this->GetCameraComponent()->FieldOfView = 60.0f;
}



AGameCamera::~AGameCamera()
{
	_registeredActor.Empty();
}

void AGameCamera::Tick(float DeltaTime)
{
	if (CalculateCameraFocusPoint())
	{
		float multiplier = CalculateOffsetMultiplier();

		float x = _vertCenter + (_minVertOffset * multiplier);
		float z = (_minHeightOffset * multiplier);

		

		SetActorLocation(FMath::Lerp(GetActorLocation(), FVector(x, _horCenter, z), _lerpAlpha));

		// Reset the pointers
		_topActor = NULL;
		_bottomActor = NULL;
		_leftActor = NULL;
		_rightActor = NULL;
	}
	
}

///<summary>
/// Calculate the mid-point for the camera focus
///</summary>
/// <returns>True if there is a focus point, which also means zero registered actor, false if otherwise</returns>
bool AGameCamera::CalculateCameraFocusPoint()
{
	bool move = false;

	if (_registeredActor.Num() > 0)
	{
		CalculateVerticalCenter();
		CalculateHorizontalCenter();
		move = true;
	}
	
	return move;
}


///<summary>
/// Calculates the camera's vertical focus point
///</summary>
void AGameCamera::CalculateVerticalCenter()
{
	if (_registeredActor.Num() > 0)
	{
		_topActor = _registeredActor[0];
		_bottomActor = _registeredActor[0];

		for (int i = 1; i < _registeredActor.Num(); i++)
		{
			// Compare X value
			if (_topActor->GetActorLocation().X < _registeredActor[i]->GetActorLocation().X)
			{
				_topActor = _registeredActor[i];
			}
			else if (_bottomActor->GetActorLocation().X > _registeredActor[i]->GetActorLocation().X)
			{
				_bottomActor = _registeredActor[i];
			}
		}

		_vertCenter = (_topActor->GetActorLocation().X + _bottomActor->GetActorLocation().X) / 2;
	}
}

///<summary>
/// Calculates the camera's horizontal focus point
///</summary>
void AGameCamera::CalculateHorizontalCenter()
{
	if (_registeredActor.Num() > 0)
	{
		_leftActor = _registeredActor[0];
		_rightActor = _registeredActor[0];

		for (int i = 1; i < _registeredActor.Num(); i++)
		{
			// Compare Y value
			if (_leftActor->GetActorLocation().Y > _registeredActor[i]->GetActorLocation().Y)
			{
				_leftActor = _registeredActor[i];
			}
			else if (_rightActor->GetActorLocation().Y < _registeredActor[i]->GetActorLocation().Y)
			{
				_rightActor = _registeredActor[i];
			}
		}

		_horCenter = (_leftActor->GetActorLocation().Y + _rightActor->GetActorLocation().Y) / 2;
	}
}


float AGameCamera::CalculateOffsetMultiplier()
{
	float multiplier = 1.0f;

	float verticalDistance = _topActor->GetActorLocation().X - _bottomActor->GetActorLocation().X;
	float horizontalDistance = _rightActor->GetActorLocation().Y - _leftActor->GetActorLocation().Y;

	if (verticalDistance > _minVertActorDist || horizontalDistance > _minHorActorDist)
	{
		// Calculate offset multiplier
		float vertMultiplier = verticalDistance / _minVertActorDist;
		float horMultiplier = horizontalDistance / _minHorActorDist;

		if (vertMultiplier > horMultiplier)
		{
			multiplier = vertMultiplier;
		}
		else
		{
			multiplier = horMultiplier;
		}
	}

	return multiplier;
}


///<summary>
/// Registers an actor to be included in the camera focus
///</summary>
///<param name="inPlayerController">Actor to be registered</param>
void AGameCamera::RegisterActor(AActor* inActor)
{
	_registeredActor.Add(inActor);
}


///<summary>
/// Removes an actor from being included in the camera focus
///</summary>
///<param name="inPlayerController">Actor to be removed</param>
void AGameCamera::RemoveActor(AActor* inActor)
{
	_registeredActor.Remove(inActor);
}
