// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "SorceryParty_GameInstance.h"
#include "SorceryParty.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuModeBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SORCERYPARTY_API AMainMenuModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	
protected:
	/** List all the levels that will be loaded **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<FName> _levelNames;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) int _playerCount = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<APlayerController*> _controllers;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<FParticipantInfo> _participatingPlayers;



	UPROPERTY(EditAnywhere, BlueprintReadWrite)  USorceryParty_GameInstance* _gameInstance;
	
public:
	UFUNCTION(BlueprintCallable) virtual void UploadLevelsToGameInstance();
	UFUNCTION(BlueprintCallable) virtual void CreatePlayerControllers();

	UFUNCTION(BlueprintCallable) virtual void Participate(APlayerController* inPlayerController);

	UFUNCTION(BlueprintCallable) virtual void BackOut(APlayerController* inPlayerController);
};
