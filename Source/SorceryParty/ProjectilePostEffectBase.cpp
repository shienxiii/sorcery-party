// Author: Thau Shien Hsu

#include "ProjectilePostEffectBase.h"
#include "SorcererCharacterBase.h"

AProjectilePostEffectBase::AProjectilePostEffectBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_sfx = CreateDefaultSubobject<UAudioComponent>(TEXT("SFX"));
	_sfx->SetupAttachment(RootComponent);
}

void AProjectilePostEffectBase::Tick(float DeltaTime)
{
	if (_attachedActor != NULL)
	{
		SetActorLocation(_attachedActor->GetActorLocation());
	}

	if (_activeTime > 0.0f)
	{
		_activeTime -= DeltaTime;
	}
	else
	{
		Destroy();
	}
}


///<summary>
/// Cause damage to the player when required and attach the particle to the actor
///</summary>
///<param name="inPlayerController">The player controller to add the the game</param>
void AProjectilePostEffectBase::AttachTo(AActor* inActor, bool toDamage, float inDamageVal)
{
	_attachedActor = inActor;

	if (inActor->IsA<ASorcererCharacterBase>() && toDamage)
	{
		((ASorcererCharacterBase*)inActor)->ReceiveDamage(inDamageVal);
	}
}
