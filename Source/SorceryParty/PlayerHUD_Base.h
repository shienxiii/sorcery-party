// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "SorcererCharacterBase.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUD_Base.generated.h"

/**
 * 
 */
UCLASS()
class SORCERYPARTY_API UPlayerHUD_Base : public UUserWidget
{
	GENERATED_BODY()

	protected:
		UPROPERTY(EditAnywhere, BlueprintReadWrite) ASorcererCharacterBase* _bindedCharacter;
	
	public:
		UFUNCTION(BlueprintCallable) void BindCharacter(ASorcererCharacterBase* _inCharacter);
};
