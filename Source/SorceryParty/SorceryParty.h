// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SorcererCharacterBase.h"
#include "Engine.h"
#include "SorceryParty.generated.h"

USTRUCT(BlueprintType)
struct FParticipantInfo
{
	GENERATED_USTRUCT_BODY()

public:
	// Player Identification
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int _playerID = -1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _score = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _playerPalette;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _crystalTagMat;

	// Player HUD
	UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _playerThumbnail;
};