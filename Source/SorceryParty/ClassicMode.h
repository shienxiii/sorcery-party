// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "SorceryParty_GameInstance.h"
#include "Engine.h"
#include "GameCamera.h"
#include "SorcererCharacterBase.h"
#include "CrystalBase.h"
#include "GameFramework/GameModeBase.h"
#include "ClassicMode.generated.h"

/**
 * Aside from doing the basic GameMode tasks, this class will also be responsible for
 * spawning the crystal game object which the players will interact with
 */
UCLASS(Blueprintable)
class SORCERYPARTY_API AClassicMode : public AGameModeBase
{
	GENERATED_BODY()
	
	protected:
		//UPROPERTY(EditAnywhere, BlueprintReadWrite) UUserWidget* _gameUI;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) USorceryParty_GameInstance* _gameInstance;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<APlayerController*> _playerControl;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<APlayerStart*> _playerStart;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<ASorcererCharacterBase*> _spawnedPlayers;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) AGameCamera* _gameCamera;
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite) TSubclassOf<class ASorcererCharacterBase> _playerCharacter;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) int _playerCount = 4;
		

		// Counter for number of spawned player
		// int _spawned = 0;

		// Crystal variables

		// Cooldown timer between crystal spawn, necessary to pace the gameplay
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") float _cooldownTime = 10.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") float _currentCooldownTime = 10.0f;

		// Timer for how long crystal will stay before burst
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") float _activeTime = 60.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") float _currentActiveTime = 0.0f;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") int _remainingCrystal = 3;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") ACrystalBase* _crystal;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crystal") TArray<APlayerStart*> _crystalSpawnPoint;

		

	public:
		AClassicMode();
		~AClassicMode();
		virtual void Tick(float DeltaTime) override;

		UFUNCTION(BlueprintCallable) virtual void InitializeLevel(TSubclassOf<APlayerStart> inPlayerStart);

		UFUNCTION(BlueprintCallable) virtual ASorcererCharacterBase* SpawnCharacter(APlayerController* inPlayerController, int inPlayerID, float inMana, UMaterialInstance* inPalette, UMaterialInstance* inCrystalTagMat, UMaterialInstance* inThumbnail);

		UFUNCTION(BlueprintCallable) virtual void DestroyCharacter(APlayerController* inPlayerController);

		UFUNCTION(BlueprintCallable) virtual void SetCamera(AGameCamera* inCamera);
		
		UFUNCTION(BlueprintCallable) virtual void RevealCrystal();
};
