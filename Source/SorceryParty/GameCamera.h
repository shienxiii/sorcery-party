// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "Camera/CameraActor.h"
#include "GameCamera.generated.h"

/**
 * 
 */
UCLASS()
class SORCERYPARTY_API AGameCamera : public ACameraActor
{
	GENERATED_BODY()
	
	
protected:
	/// Actors registered to an AFollowCamera instance will be included
	/// in the calculation for the camera movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<AActor*> _registeredActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _lerpAlpha = 0.1f;

	// Variables to determine which actor is furthest to each 4 direction
	AActor* _topActor = NULL;
	AActor* _bottomActor = NULL;
	AActor* _leftActor = NULL;
	AActor* _rightActor = NULL;

	// Screen ratio
	float _sWidth = 16.0f;
	float _sHeight = 9.0f;

	// Variables to determine the center point in which the the camera's view focus should be on
	float _vertCenter;
	float _horCenter;

	float _vertOffset;
	float _heightOffset;

	

	//UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minVertActorDist = 2000.0f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minHorActorDist = 4000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minVertActorDist = 1600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minHorActorDist = 2500.0f;

	// These are the minimum camera location offset
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minVertOffset = -1600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _minHeightOffset = 2500.0f;

	bool CalculateCameraFocusPoint();

	void CalculateVerticalCenter();
	void CalculateHorizontalCenter();

	float CalculateOffsetMultiplier();


public:
	AGameCamera();
	~AGameCamera();
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable) void RegisterActor(AActor* inActor);
	UFUNCTION(BlueprintCallable) void RemoveActor(AActor* inActor);

	
	
};
