// Author: Thau Shien Hsu

#include "ScoreboardBase.h"

///<summary>
/// Go through the participating players to sort them by order
///</summary>
void UScoreboardBase::SortScores()
{
	if (_gameInstance == NULL)
	{
		_gameInstance = (USorceryParty_GameInstance*)UGameplayStatics::GetGameInstance(GetWorld());
	}

	for (int i = 0; i < _gameInstance->GetParticipatingPlayers().Num(); i++)
	{
		if (_gameInstance->GetParticipatingPlayers()[i]._playerID != -1)
		{
			//_participatingPlayers.Add(_gameInstance->GetParticipatingPlayers()[i]);
			InsertSort(_gameInstance->GetParticipatingPlayers()[i]);
		}
	}
	
}

///<summary>
/// Insert the player data into the score list, sorted by score
///</summary>
///<param name="inPlayer">The player data</param>
void UScoreboardBase::InsertSort(FParticipantInfo inPlayer)
{
	if (_participatingPlayers.Num() == 0)
	{
		_participatingPlayers.Add(inPlayer);
	}
	else
	{
		int index = 0;

		bool found = false;
		
		for (int i = 0; i < _participatingPlayers.Num() && !found; i++)
		{
			if (_participatingPlayers[i]._score < inPlayer._score)
			{
				_participatingPlayers.Insert(inPlayer, i);
				found = true;
			}
		}

		if (!found)
		{
			_participatingPlayers.Add(inPlayer);
		}
	}
}
