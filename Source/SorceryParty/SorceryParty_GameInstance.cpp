// Author: Thau Shien Hsu

#include "SorceryParty_GameInstance.h"


void USorceryParty_GameInstance::AddLevel(FName inLevelName)
{
	_levelNames.Add(inLevelName);
}

///<summary>
/// Reset the game instance so that the game can be played again
///</summary>
void USorceryParty_GameInstance::ResetGameInstance()
{
	//RemoveAllLevel();

	for (int i = 0; i < _participatingPlayers.Num(); i++)
	{
		_participatingPlayers[i]._score = 0;
	}
	
	//_participatingPlayers.Empty();
}


void USorceryParty_GameInstance::RemoveAllLevel()
{
	//_levelNames
	_levelNames.Empty();
}

///<summary>
/// Save player score to game instance
///</summary>
void USorceryParty_GameInstance::SavePlayerScore()
{
	for (int i = 0; i < _participatingPlayers.Num(); i++)
	{
		if (_participatingPlayers[i]._playerID != -1)
		{
			//found = true;
			//_participatingPlayers[i]._playerID = (int)UGameplayStatics::GetPlayerControllerID(inPlayerController);
			ASorcererCharacterBase* player = (ASorcererCharacterBase*)UGameplayStatics::GetPlayerController(GetWorld(), _participatingPlayers[i]._playerID)->GetPawn();
			if (player != NULL)
			{
				_participatingPlayers[i]._score = player->GetMana();
			}
		}
	}
}

///<summary>
/// Save player score to game instance and load the next level, if there are no more level, load the result screen
///</summary>
void USorceryParty_GameInstance::LoadNextLevel()
{
	if (_levelNames.Num() > 0)
	{
		SavePlayerScore();

		int i = FMath::RandRange(0, _levelNames.Num() - 1);
		FName name = _levelNames[i];
		_levelNames.RemoveAt(i);
		UGameplayStatics::OpenLevel(GetWorld(), name);
	}
	else
	{
		EndGame();
	}
}

///<summary>
/// Save player score to game instance and end the game
///</summary>
void USorceryParty_GameInstance::EndGame()
{
	SavePlayerScore();

	UGameplayStatics::OpenLevel(GetWorld(), _resultScreenMapName);
}

///<summary>
/// Add a new slot for players to be able to participate in the game
///</summary>
void USorceryParty_GameInstance::AddPlayerSlot(UMaterialInstance* inPlayerPalette, UMaterialInstance* inCrystalTagMat, UMaterialInstance* inPlayerThumbnail)
{
	FParticipantInfo newParticipant;
	newParticipant._playerID = -1;
	newParticipant._playerPalette = inPlayerPalette;
	newParticipant._crystalTagMat = inCrystalTagMat;
	newParticipant._playerThumbnail = inPlayerThumbnail;

	_participatingPlayers.Add(newParticipant);
}

///<summary>
/// Register the player controller input for the next available seat for the next game
///</summary>
///<param name="inPlayerController">The player controller</param>
bool USorceryParty_GameInstance::Participate(APlayerController* inPlayerController)
{
	bool found = false;

	for (int i = 0; (i < _participatingPlayers.Num() && !found); i++)
	{
		if (_participatingPlayers[i]._playerID == -1)
		{
			found = true;
			_participatingPlayers[i]._playerID = (int)UGameplayStatics::GetPlayerControllerID(inPlayerController);
		}
		else if (_participatingPlayers[i]._playerID == (int)UGameplayStatics::GetPlayerControllerID(inPlayerController))
		{
			found = true;
		}
	}

	return found;
}

///<summary>
/// Remove the player controller input for the next available seat for the next game
///</summary>
///<param name="inPlayerController">The player controller</param>
bool USorceryParty_GameInstance::BackOut(APlayerController * inPlayerController)
{
	bool found = false;

	for (int i = 0; (i < _participatingPlayers.Num() && !found); i++)
	{
		if (_participatingPlayers[i]._playerID == (int)UGameplayStatics::GetPlayerControllerID(inPlayerController))
		{
			_participatingPlayers[i]._playerID = -1;
			found = true;
		}
	}

	return found;
}

///<summary>
/// Return the list for all the players participating in the game
///</summary>
TArray<FParticipantInfo> USorceryParty_GameInstance::GetParticipatingPlayers()
{
	return _participatingPlayers;
}
