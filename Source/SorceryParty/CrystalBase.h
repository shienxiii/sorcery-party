// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "SorcererCharacterBase.h"
#include "GameCamera.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "CrystalBase.generated.h"

UCLASS()
class SORCERYPARTY_API ACrystalBase : public AActor
{
	GENERATED_BODY()
	
	protected:
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UCapsuleComponent* _collider;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UStaticMeshComponent* _crystalMesh;

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UParticleSystemComponent* _idleParticle;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UParticleSystemComponent* _taggerLinkParticle;

		// Audio
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _appearSound;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _disappearSound;

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UParticleSystem* _idleParticleTemplate;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) ASorcererCharacterBase* _tagger;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) AGameCamera* _gameCamera;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _defaultMat;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score") float _manaPerSecond = 1.0f;
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Sets default values for this actor's properties
		ACrystalBase();
		// Called every frame
		virtual void Tick(float DeltaTime) override;

		virtual void TimeOut();

		UFUNCTION(BlueprintCallable) virtual void DeactivateIdleParticle();
		UFUNCTION(BlueprintCallable) virtual void RevealCrystal();
		UFUNCTION(BlueprintCallable) virtual void HideCrystal();
		
		UFUNCTION(BlueprintCallable) virtual void Tagged(ASorcererCharacterBase* inTagger);
		//virtual void AddTagBonus();
		UFUNCTION(BlueprintCallable) virtual void RemoveTaggedPlayer();

		/** called when something enters the collider */
		UFUNCTION(BlueprintCallable) void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

		/** called when something leaves the collider */
		//UFUNCTION() void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
