// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "GameCamera.h"
#include "Projectiles.h"
#include "Engine.h"
#include "GameFramework/Character.h"
#include "SorcererCharacterBase.generated.h"

UCLASS(Blueprintable)
class SORCERYPARTY_API ASorcererCharacterBase : public ACharacter
{
	GENERATED_BODY()

	protected:

		UPROPERTY(BlueprintReadOnly) AGameCamera* _gameCamera;
		UPROPERTY(BlueprintReadOnly) APlayerController* _controllingPlayer;

		// Where to spawn character
		APlayerStart* _spawnPoint;

		// Basic stat
		UPROPERTY(EditAnywhere, BlueprintReadOnly) float _hp = 100.0f;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) float _mana = 0.0f;

		// HUD Thumbnail
		UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _thumbnail = NULL;

		float _maxHP = 100.0f;
		float _maxMana = 100.f;

		UPROPERTY(BlueprintReadOnly) float _movementSpeed;
		UPROPERTY(BlueprintReadOnly) float _movementSpeedMod = 1.5f;

		UPROPERTY(BlueprintReadOnly) bool _tookDamage = false;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death") float _downTimerCurrent;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death") float _downTimerBase = 1.0f;

		// Magic
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setup") UStaticMeshComponent* _magicCircle;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) TSubclassOf<class AProjectiles> _projectile;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) bool _cast = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) float _castCurrentCooldown = 0.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) float _castCooldown = 0.5;

		UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _crystalTagMat = NULL;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _magicIcon = NULL;

		// Particle System
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UParticleSystemComponent* _playerAura;
		// Audio
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _locomotionSound;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _castingSound;

		bool isHidden = false;

		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Sets default values for this character's properties
		ASorcererCharacterBase();

		// Called every frame
		virtual void Tick(float DeltaTime) override;
		virtual void CastingUpdate(float DeltaTime);

		// Called to bind functionality to input
		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

		virtual void PossessedBy(AController* NewController) override;


		UFUNCTION(BlueprintCallable, Category = "Setup") virtual void SpawnInitialization(AGameCamera* inGameCamera, APlayerStart* inSpawnPoint, UMaterialInstance* inThumbnail, float inMana);

		// Crystal tagging event
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void CrystalTagEvent();
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void CrystalUntagEvent();
		UFUNCTION(BlueprintPure, Category = "SorcererCharacterBase") virtual float AddMana(float inMana);

		UFUNCTION(BlueprintPure, Category = "SorcererCharacterBase") virtual UMaterialInstance* GetCrystalTagMat();
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void SetCrystalTagMat(UMaterialInstance* inCrystalTagMat);

		// Magic
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void Cast();
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void ChangeMagic(UMaterialInterface* inMagicCircleMat, TSubclassOf<AProjectiles> inMagicProjectile);
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void ReceiveDamage(float inDamage);

		// Recovery/Support functions
		//UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void Heal(float inRecovery);
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void DamageAnimComplete();

		// Defeat effect
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void Incapacitated();
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void DisableCharacter();
		UFUNCTION(BlueprintCallable, Category = "SorcererCharacterBase") virtual void ResetStats();

		UFUNCTION(BlueprintPure, Category = "Player Stats") virtual float GetHPRatio();
		UFUNCTION(BlueprintPure, Category = "Player Stats") virtual float GetManaRatio();
		UFUNCTION(BlueprintPure, Category = "Player Stats") virtual float GetMana();
		UFUNCTION(BlueprintPure, Category = "SorcererCharacterBase") virtual AProjectiles* GetProjectileClassReference();
};
