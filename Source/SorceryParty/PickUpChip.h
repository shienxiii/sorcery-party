// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Projectiles.h"
#include "GameFramework/Actor.h"
#include "PickUpChip.generated.h"

UCLASS(Blueprintable)
class SORCERYPARTY_API APickUpChip : public AActor
{
	GENERATED_BODY()
	
	protected:
		// Variables for Magic Chips
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic Chip") UMaterialInterface* _magicCircleMat = NULL;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic Chip") TSubclassOf<AProjectiles> _magicProjectile = NULL;

		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Sets default values for this actor's properties
		APickUpChip();
		// Called every frame
		virtual void Tick(float DeltaTime) override;
		

};
