// Author: Thau Shien Hsu

#include "PlayerHUD_Base.h"

///<summary>
/// Bind the HUD to the player character
///</summary>
void UPlayerHUD_Base::BindCharacter(ASorcererCharacterBase * _inCharacter)
{
	_bindedCharacter = _inCharacter;
}
