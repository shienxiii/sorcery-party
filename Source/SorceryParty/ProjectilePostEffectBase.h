// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Particles/Emitter.h"
#include "ProjectilePostEffectBase.generated.h"

/**
 * 
 */
UCLASS()
class SORCERYPARTY_API AProjectilePostEffectBase : public AEmitter
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _sfx = NULL;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) AActor* _attachedActor = NULL;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float _activeTime = 1.0f;

	

public:
	AProjectilePostEffectBase();
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable) virtual void AttachTo(AActor* inActor, bool toDamage = false, float inDamageVal = 0.0f);
	
};
