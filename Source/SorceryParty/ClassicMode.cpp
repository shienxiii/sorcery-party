// Author: Thau Shien Hsu

#include "ClassicMode.h"
#include "SorceryParty.h"



AClassicMode::AClassicMode()
{
	// Obtain the player character blueprint
	ConstructorHelpers::FObjectFinder<UClass> PawnBlueprint(TEXT("Class'/Game/Blueprints/PlayerBP/Game/SorcererCharacter.SorcererCharacter_C'"));
	_playerCharacter = (UClass*)PawnBlueprint.Object;
	// PUT TO DEV NOTE
	
}

/*Class Destructor*/
AClassicMode::~AClassicMode()
{
	_playerStart.Empty();
	_playerControl.Empty();
}



void AClassicMode::Tick(float DeltaTime)
{
	if (_currentCooldownTime > 0.0f)
	{
		// Cooling down
		_currentCooldownTime -= DeltaTime;
	}
	else if (_currentCooldownTime <= 0.0f && _currentActiveTime <= 0.0f && !_crystal->IsActorTickEnabled())
	{
		
		if (_remainingCrystal == 0)
		{
			// Change level
			_gameInstance->LoadNextLevel();
		}
		else
		{
			// Cooldown finish, reveal crystal
			
			_currentActiveTime = _activeTime;
			RevealCrystal();
		}
	}
	else if (_currentCooldownTime <= 0.0f && _currentActiveTime > 0.0f && _crystal->IsActorTickEnabled())
	{
		// Countdown to hide crystal
		_currentActiveTime -= DeltaTime;

		if (_currentActiveTime <= 5.0f)
		{
			_crystal->DeactivateIdleParticle();
		}
	}
	else if (_currentCooldownTime <= 0.0f && _currentActiveTime <= 0.0f && _crystal->IsActorTickEnabled())
	{
		// Hide crystal
		_currentCooldownTime = _cooldownTime;
		_remainingCrystal--;
		_crystal->HideCrystal();
		_gameCamera->RemoveActor(_crystal);
			
	}
	
}

///<summary>
/// Initialize the game mode with all the details needed for this level, also spawns the game camera
///</summary>
///<param name="inPlayerStart">The PlayerStart class to look for</param>
void AClassicMode::InitializeLevel(TSubclassOf<APlayerStart> inPlayerStart)
{

	TArray<AActor*> playerStart;

	// Get all placed player start position
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), inPlayerStart, playerStart);
	
	for (int i = 0; i < playerStart.Num(); i++)
	{
		_playerStart.Add((APlayerStart*)playerStart[i]);
	}

	_gameInstance = (USorceryParty_GameInstance*)UGameplayStatics::GetGameInstance(GetWorld());

	// Initialize player control
	_playerControl.Add(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	
	
	while (_playerControl.Num() < _playerCount)
	{
		APlayerController* control = UGameplayStatics::CreatePlayer(GetWorld(), -1, true);

		_playerControl.Add(UGameplayStatics::GetPlayerController(GetWorld(), _playerControl.Num()));
	}

	// Spawn game camnera
	_gameCamera = GetWorld()->SpawnActor<AGameCamera>(FVector(-600.0f, 0.0f, 1000.0f), FRotator(-60.0f, 0.0f, 0.0f));
	_playerControl[0]->SetViewTarget(_gameCamera);

	

	
	// Spawn player character for all participating player
	for (int i = 0; i < _gameInstance->GetParticipatingPlayers().Num(); i++)
	{
		if (_gameInstance->GetParticipatingPlayers()[i]._playerID > -1)
		{
			ASorcererCharacterBase* spawned = SpawnCharacter(_playerControl[_gameInstance->GetParticipatingPlayers()[i]._playerID],
				_gameInstance->GetParticipatingPlayers()[i]._playerID,
				_gameInstance->GetParticipatingPlayers()[i]._score,
				_gameInstance->GetParticipatingPlayers()[i]._playerPalette,
				_gameInstance->GetParticipatingPlayers()[i]._crystalTagMat,
				_gameInstance->GetParticipatingPlayers()[i]._playerThumbnail);

			
			_spawnedPlayers.Add(spawned);

			
		}
	}


}


///<summary>
/// Spawn the player character to be controlled by the input player controller
///</summary>
///<param name="inPlayerController">Player Controller this character is spawned for</param>
ASorcererCharacterBase* AClassicMode::SpawnCharacter(APlayerController* inPlayerController, int inPlayerID, float inMana, UMaterialInstance* inPalette, UMaterialInstance* inCrystalTagMat, UMaterialInstance* inThumbnail)
{
	ASorcererCharacterBase* spawnedPawn = NULL;

	if (!inPlayerController->GetPawn() && _playerCharacter != NULL)
	{
		spawnedPawn = GetWorld()->SpawnActor<ASorcererCharacterBase>(_playerCharacter, _playerStart[inPlayerID]->GetActorLocation(), _playerStart[inPlayerID]->GetActorRotation());
		_gameCamera->RegisterActor(spawnedPawn);

		
		spawnedPawn->SpawnInitialization(_gameCamera, _playerStart[inPlayerID], inThumbnail, inMana);
		
		spawnedPawn->GetMesh()->SetMaterial(0, inPalette);
		spawnedPawn->SetCrystalTagMat(inCrystalTagMat);
		

		inPlayerController->Possess(spawnedPawn);
		

		inPlayerController->SetViewTarget(_gameCamera);
	}
	
	return spawnedPawn;
}



///<summary>
/// Destroy the pawn possessed by the player controller
///</summary>
///<param name="inPlayerController">Player Controller whose pawn to be destroyed</param>
void AClassicMode::DestroyCharacter(APlayerController* inPlayerController)
{
	if (inPlayerController->GetPawn())
	{
		APawn* pawn = inPlayerController->GetPawn();
		_gameCamera->RemoveActor(pawn);
		inPlayerController->UnPossess();
		pawn->Destroy();

		inPlayerController->SetViewTarget(_gameCamera);

	}
}


///<summary>
/// Set the game camera
///</summary>
///<param name="inCamera">Game Camera to use</param>
void AClassicMode::SetCamera(AGameCamera* inCamera)
{
	_gameCamera = inCamera;
}


///<summary>
/// Reveal the crystal
///</summary>
void AClassicMode::RevealCrystal()
{
	if (_currentCooldownTime <= 0.0f)
	{
		int i = FMath::RandRange(0, _crystalSpawnPoint.Num() - 1);

		_crystal->SetActorTransform(_crystalSpawnPoint[i]->GetActorTransform());

		
		_crystal->RevealCrystal();

		

		_gameCamera->RegisterActor(_crystal);
	}
}