// Author: Thau Shien Hsu


#include "CrystalBase.h"
#include "SorceryParty_GameInstance.h"


// Sets default values
ACrystalBase::ACrystalBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = false;
	SetActorTickEnabled(false);
	_collider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collider"));
	_collider->SetCapsuleHalfHeight(175.0f);
	_collider->SetCapsuleRadius(100.0f);
	RootComponent = _collider;

	_collider->OnComponentBeginOverlap.AddDynamic(this, &ACrystalBase::OnOverlapBegin);

	_crystalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CrystalMesh"));
	_crystalMesh->SetupAttachment(RootComponent);

	_appearSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Appear sound"));
	_appearSound->SetupAttachment(this->RootComponent);

	_disappearSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Disappear sound"));
	_disappearSound->SetupAttachment(this->RootComponent);

	_taggerLinkParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Tagger Link Particle"));
	_taggerLinkParticle->SetupAttachment(RootComponent);
	
	
}

// Called when the game starts or when spawned
void ACrystalBase::BeginPlay()
{
	Super::BeginPlay();
	
	_defaultMat = (UMaterialInstance*) _crystalMesh->GetMaterial(0);
	_taggerLinkParticle->SetActorParameter("Source", this);
	_taggerLinkParticle->Deactivate();
}

// Called every frame
void ACrystalBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (_tagger != NULL)
	{
		if (_tagger->GetHPRatio() <= 0.0)
		{
			RemoveTaggedPlayer();
		}
		else
		{
			if (_tagger->AddMana(_manaPerSecond * DeltaTime) == 100.0f)
			{
				((USorceryParty_GameInstance*)UGameplayStatics::GetGameInstance(GetWorld()))->EndGame();
			}
		}
	}
}


void ACrystalBase::TimeOut()
{
	HideCrystal();
}

void ACrystalBase::DeactivateIdleParticle()
{
	_idleParticle->Deactivate();
}

///<summary>
/// Reveal the crystal and make it able to be tagged
///</summary>
///<param name="inCamera">Game Camera to use</param>
void ACrystalBase::RevealCrystal()
{
	//_idleParticle->Activate(true);
	_idleParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), _idleParticleTemplate, GetActorTransform());

	_taggerLinkParticle->Deactivate();
	SetActorHiddenInGame(false);
	SetActorEnableCollision(true);
	SetActorTickEnabled(true);

	_appearSound->Play();
}


///<summary>
/// Hide the crystal and remove the tagger
///</summary>
void ACrystalBase::HideCrystal()
{
	RemoveTaggedPlayer();
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);
}


///<summary>
/// Called when tagged by any player
///</summary>
///<param name="inTagger">Reference to the player who tagged this crystal</param>
void ACrystalBase::Tagged(ASorcererCharacterBase* inTagger)
{
	RemoveTaggedPlayer();

	_tagger = inTagger;
	_tagger->CrystalTagEvent();

	if (_taggerLinkParticle != NULL)
	{
		
		_taggerLinkParticle->SetActorParameter("Target", inTagger);
		_taggerLinkParticle->Activate();
	}
	
	_crystalMesh->SetMaterial(0, (UMaterialInterface*) _tagger->GetCrystalTagMat());
}

///<summary>
/// Called whenever there is an event that causes the current tagger to lose ownership of the crystal
///</summary>
void ACrystalBase::RemoveTaggedPlayer()
{
	if (_tagger != NULL)
	{
		_taggerLinkParticle->Deactivate();

		_tagger->CrystalUntagEvent();
		_tagger = NULL;
		_crystalMesh->SetMaterial(0, _defaultMat);

		if (_taggerLinkParticle != NULL)
		{
			_taggerLinkParticle->SetActorParameter("Target", NULL);
			
		}
	}
}

void ACrystalBase::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	/*
	if (OtherActor->IsA(AProjectiles::StaticClass()))
	{
		AProjectiles* projectile = (AProjectiles*)OtherActor;

		Tagged((ASorcererCharacterBase*)projectile->GetCaster());
		


		projectile->Destroy();

	}
	*/

}

