// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "Projectiles.generated.h"

UCLASS(Blueprintable)
class SORCERYPARTY_API AProjectiles : public AActor
{
	GENERATED_BODY()
	
	protected:
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) USphereComponent* _collider = NULL;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UStaticMeshComponent* _mesh = NULL;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UParticleSystemComponent* _particleEffect = NULL;

		// User Experience
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _castSfx = NULL;
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite) UAudioComponent* _movingSfx = NULL;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite) UParticleSystem* _explosion = NULL;
		UPROPERTY(EditAnywhere, BlueprintReadWrite) TSubclassOf<class AEmitter> _explosion = NULL;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Stat") float _damage = 10.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Projectile Stat") float _speed = 3000.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Stat") float _range = 5000.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Stat") float _pushbackForce = 1000.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Stat") ACharacter* _caster = NULL;

		// HUD Info
		UPROPERTY(EditAnywhere, BlueprintReadWrite) UMaterialInstance* _icon = NULL;

		

		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		
		AProjectiles(); // Sets default values for this actor's properties
		virtual void Tick(float DeltaTime) override;// Called every frame

		virtual void Initialize(ACharacter* inCaster);

		UFUNCTION(BlueprintPure) ACharacter* GetCaster();

		/** called when something enters the collider */
		UFUNCTION(BlueprintCallable) void OverlapEvent(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, bool inDestroy = true);

		/** called when something leaves the collider */
		//UFUNCTION() void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	
};
