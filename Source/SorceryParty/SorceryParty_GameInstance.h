// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "SorceryParty.h"
#include "Engine.h"
#include "Engine/GameInstance.h"
#include "SorceryParty_GameInstance.generated.h"



/**
 * 
 */
UCLASS()
class SORCERYPARTY_API USorceryParty_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<FName> _levelNames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) FName _resultScreenMapName = "ResultScreen";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<FParticipantInfo> _participatingPlayers;
	
public:
	UFUNCTION(BlueprintCallable) virtual void AddLevel(FName inLevelName);
	UFUNCTION(BlueprintCallable) virtual void ResetGameInstance();
	UFUNCTION(BlueprintCallable) virtual void RemoveAllLevel();

	UFUNCTION(BlueprintCallable) virtual void SavePlayerScore();
	UFUNCTION(BlueprintCallable) virtual void LoadNextLevel();
	UFUNCTION(BlueprintCallable) virtual void EndGame();

	UFUNCTION(BlueprintCallable) virtual void AddPlayerSlot(UMaterialInstance* inPlayerPalette, UMaterialInstance* inCrystalTagMat, UMaterialInstance* inPlayerThumbnail);

	UFUNCTION(BlueprintPure) virtual bool Participate(APlayerController* inPlayerController);
	UFUNCTION(BlueprintPure) virtual bool BackOut(APlayerController* inPlayerController);

	UFUNCTION(BlueprintCallable) virtual TArray<FParticipantInfo> GetParticipatingPlayers();
};

