// Author: Thau Shien Hsu

#include "MainMenuModeBase.h"
#include "SorceryParty_GameInstance.h"

// Load all levels in the _levelNames into the game instance
void AMainMenuModeBase::UploadLevelsToGameInstance()
{
	_levelNames.Add("Arena");
	_levelNames.Add("WalledArena");
	_levelNames.Add("MirrorIsland");

	USorceryParty_GameInstance* gameInstance = (USorceryParty_GameInstance*) UGameplayStatics::GetGameInstance(GetWorld());
	gameInstance->RemoveAllLevel();
	
	for (int i = 0; i < _levelNames.Num(); i++)
	{
		gameInstance->AddLevel(_levelNames[i]);
	}
}


void AMainMenuModeBase::CreatePlayerControllers()
{
	for (int i = 0; i < _playerCount; i++)
	{
		if (UGameplayStatics::GetPlayerController(GetWorld(), i) != NULL)
		{
			_controllers.Add(UGameplayStatics::GetPlayerController(GetWorld(), i));
		}
		else
		{
			_controllers.Add(UGameplayStatics::CreatePlayer(GetWorld(), i));
		}

		
		_controllers[i]->SetInputMode(FInputModeGameOnly());
	}
	//_controllers[0]->bShowMouseCursor = true;

}


///<summary>
/// Registers the input player controller
///</summary>
///<param name="inPlayerController">The player controller to add to the game</param>
void AMainMenuModeBase::Participate(APlayerController* inPlayerController)
{
	_gameInstance->Participate(inPlayerController);
}


///<summary>
/// Remove the input player controller
///</summary>
///<param name="inPlayerController">The player controller to removefrom the game</param>
void AMainMenuModeBase::BackOut(APlayerController* inPlayerController)
{
	_gameInstance->BackOut(inPlayerController);
}