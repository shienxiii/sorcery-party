// Author: Thau Shien Hsu

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "SorceryParty_GameInstance.h"
#include "Blueprint/UserWidget.h"
#include "ScoreboardBase.generated.h"

/**
 * 
 */
UCLASS()
class SORCERYPARTY_API UScoreboardBase : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) USorceryParty_GameInstance* _gameInstance = NULL;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default) TArray<FParticipantInfo> _participatingPlayers;
	
public:
	UFUNCTION(BlueprintCallable) virtual void SortScores();
	virtual void InsertSort(FParticipantInfo inPlayer);
};
