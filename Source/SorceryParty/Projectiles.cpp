// Author: Thau Shien Hsu

#include "Projectiles.h"
#include "SorcererCharacterBase.h"
#include "CrystalBase.h"


// Sets default values
AProjectiles::AProjectiles()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	_collider = CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
	_collider->SetSphereRadius(75.0f);
	RootComponent = _collider;

	//_collider->OnComponentBeginOverlap.AddDynamic(this, &AProjectiles::OnOverlapBegin);

	_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	_mesh->SetupAttachment(RootComponent);

	_particleEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Effect"));
	_particleEffect->SetupAttachment(RootComponent);

	_castSfx = CreateDefaultSubobject<UAudioComponent>(TEXT("Cast Sound"));
	_castSfx->SetupAttachment(RootComponent);

	_movingSfx = CreateDefaultSubobject<UAudioComponent>(TEXT("Moving Sound"));
	_movingSfx->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AProjectiles::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectiles::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float frameDistance = _speed * DeltaTime;

	if (_range > 0)
	{
		AddActorWorldOffset(GetActorForwardVector() * frameDistance);
		_range -= frameDistance;
	}
	else
	{
		Destroy();
	}
}

///<summary>
/// Initialize the projectile by making sure it remembers the character that cast it
///</summary>
///<param name="inCaster">The caster that casted this projectile</param>
void AProjectiles::Initialize(ACharacter * inCaster)
{
	_caster = inCaster;
	
}


ACharacter* AProjectiles::GetCaster()
{
	return _caster;
}


///<summary>
/// The overlap event for this projectile, have same parameter as OnOverlapBegin()
///</summary>
void AProjectiles::OverlapEvent(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, bool inDestroy)
{
	if (OtherActor->IsA(ACrystalBase::StaticClass()))
	{
		if (_caster != NULL)
		{
			((ACrystalBase*)OtherActor)->Tagged((ASorcererCharacterBase*)_caster);
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Keep your distance"));
		}
	}
	else if (OtherActor->IsA(ASorcererCharacterBase::StaticClass()))
	{

		((ASorcererCharacterBase*)OtherActor)->LaunchCharacter(GetActorForwardVector() * _pushbackForce, true, false);

		((ASorcererCharacterBase*)OtherActor)->ReceiveDamage(_damage);
	}

	if (_explosion != NULL)
	{

		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), _explosion, GetActorTransform(), true);
		//AEmitter * a = GetWorld()->SpawnActor<AEmitter>()
		GetWorld()->SpawnActor<AEmitter>(_explosion, GetActorTransform());
	}

	if (inDestroy)
	{
		Destroy();
	}

}

